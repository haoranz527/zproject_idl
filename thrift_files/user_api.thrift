namespace go User

struct UserVerCodeRequest {
    1:required string Type;
    2:required string Contact;
}

struct UserVerCodeResponse {
    1:required string Status;
}

service UserService {
    UserVerCodeResponse GetVerCode(1:UserVerCodeRequest request);
}